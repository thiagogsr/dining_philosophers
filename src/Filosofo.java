public class Filosofo extends Thread {
	private int id;
	private int estado;
	private Jantar jantar;
	
	private int PENSANDO = 0;
	private int FOME     = 1;
	private int COMENDO  = 2;
	
	public Filosofo(int id, Jantar jantar, String nome) {
		super(nome);
		this.id = id;
		this.jantar = jantar;
	}
	
	public int getEstado() {
		return this.estado;
	}
	
	private int direita() {
		return (id + 1) % 5;
	}
	
	private int esquerda() {
		return (id + 5 - 1) % 5;
	}
	
	private void pensa() {
		this.estado = PENSANDO;
		System.out.println("O Filósofo " + getName() + " está pensando!");
	}
	
	private void fome() {
		this.estado = FOME;
		System.out.println("O Filósofo " + getName() + " está com fome!");
	}
	
	private void pega_hashi() {
		jantar.getMutex().decrementar();
		fome();
		tentar();
		jantar.getMutex().incrementar();
		jantar.getSemaforos()[id].decrementar();
	}
	
	private void larga_hashi() {
		jantar.getMutex().decrementar();
		pensa();
		jantar.getFilosofos()[esquerda()].tentar();
		jantar.getFilosofos()[direita()].tentar();
		jantar.getMutex().incrementar();
	}
	
	private void come() {
		estado = COMENDO;
		System.out.println("O Filósofo " + getName() + " está comendo!");

		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void tentar() {
		if (estado == FOME
				&& jantar.getFilosofos()[esquerda()].getEstado() != COMENDO
				&& jantar.getFilosofos()[direita()].getEstado() != COMENDO) {
			come();
			jantar.getSemaforos()[id].incrementar();
		}
	}
	
	private void sleep() {
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		pensa();

		while(true) {
			pega_hashi();
			sleep();
			larga_hashi();
		}
	}

}
