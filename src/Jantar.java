public class Jantar implements Runnable {
	private Semaforo mutex;
	private Semaforo semaforos[];
	private Filosofo filosofos[];
	private int      estados[];
	
	public Jantar() {
		this.mutex     = new Semaforo(1);
		this.semaforos = new Semaforo[5];
		this.filosofos = new Filosofo[5];
		this.estados   = new int[5];
	}
	
	public Filosofo[] getFilosofos() {
		return filosofos;
	}
	
	public Semaforo[] getSemaforos() {
		return semaforos;
	}
	
	public Semaforo getMutex() {
		return this.mutex;
	}

	@Override
	public void run() {
		filosofos[0] = new Filosofo(0, this, "Jose");
		filosofos[1] = new Filosofo(1, this, "Joao");
		filosofos[2] = new Filosofo(2, this, "Pedro");
		filosofos[3] = new Filosofo(3, this, "Marcos");
		filosofos[4] = new Filosofo(4, this, "Paulo");
		
		semaforos[0] = new Semaforo();
		semaforos[1] = new Semaforo();
		semaforos[2] = new Semaforo();
		semaforos[3] = new Semaforo();
		semaforos[4] = new Semaforo();
		
		filosofos[0].start();
        filosofos[1].start();
        filosofos[2].start();
        filosofos[3].start();
        filosofos[4].start();
	}
}
